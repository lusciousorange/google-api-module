<?php
class TMv_GoogleCalendarEmbed extends TCv_View
{
	use TMt_PagesContentView;

	protected $embed_HTML;
	/**
	 * TMm_GoogleCalendarFeed
	 */
	public function __construct()
	{
		parent::__construct();

		$this->addClass('responsive_iframe');
	}

	public function html()
	{

		$this->addText($this->embed_HTML);
		return parent::html();
	}

	//////////////////////////////////////////////////////
	//
	// TMt_PagesContentView TRAIT
	//
	// Settings related to the content view traits
	//
	//////////////////////////////////////////////////////

	/**
	 * Returns an array of form items to be loaded when editing this content layout
	 * @return TCv_FormItem[]
	 */
	public function pageContent_EditorFormItems()
	{
		$form_items = array();

		$title = new TCv_FormItem_TextBox('embed_HTML','Embed HTML');
		$title->setHelpText('The embed code coming directly from Google Calendar.');
		$form_items[] = $title;


		return $form_items;
	}


//<iframe src="https://calendar.google.com/calendar/b/1/embed
//	?src=paulo%40lusciousorange.com&amp;color=%230e61b9&amp;src=lusciousorange.com_62hg7kencprqdlvvn8i892ullg%40group
//.calendar.google.com&amp;color=%2329527A&amp;ctz=America%2FWinnipeg" style="border-width:0" width="800" height="600"
// frameborder="0" scrolling="no"></iframe>


	public static function pageContent_ViewTitle() { return 'Calendar – Embed'; }
	public static function pageContent_IconCode() { return 'fa-calendar'; }
	public static function pageContent_ViewDescription()
	{
		return 'An embedded google calendar that shows the next item on the calendar.';
	}
//<iframe src="https://calendar.google.com/calendar/embed
//	?src=lusciousorange.com_62hg7kencprqdlvvn8i892ullg%40group.calendar.google.com&ctz=America/Winnipeg"
//style="border: 0" width="800" height="600" frameborder="0" scrolling="no"></iframe>


// lusciousorange.com_62hg7kencprqdlvvn8i892ullg@group.calendar.google.com
}
?>
