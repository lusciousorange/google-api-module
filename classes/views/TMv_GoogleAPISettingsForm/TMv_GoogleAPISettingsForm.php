<?php
class TMv_GoogleAPISettingsForm extends TSv_ModuleSettingsForm
{
	public function __construct($module)
	{	
		parent::__construct($module);
		
	}
	
	public function configureFormElements()
	{
		$field = new TCv_FormItem_Heading('api_heading', 'Google API Settings');
		$field->setHelpText('<a href="https://developers.google.com/api-client-library/php/auth/web-app">https://developers.google.com/api-client-library/php/auth/web-app</a>');
		$this->attachView($field);


		$field = new TCv_FormItem_TextField('api_key', 'API Key');
		$field->setHelpText('A standard API Key used with many of Google services');
		$this->attachView($field);


		$field = new TCv_FormItem_TextField('client_id', 'Client ID');
		$field->setHelpText('Visit the <a href="https://console.developers.google.com/apis/credentials">Google Console</a> and create OAuth2 Credentials');
		$this->attachView($field);

		$field = new TCv_FormItem_TextField('client_secret', 'Client Secret');
		$this->attachView($field);


		// SERVICE ACCOUNTS - Used for server-to-server applications instead of authorizing different users to use
		// an app on the site.
		//https://github.com/google/google-api-php-client


		$field = new TCv_FormItem_Heading('service_heading', 'Google Service Account Settings');
		$field->setHelpText('<a href="https://developers.google.com/identity/protocols/OAuth2ServiceAccount">https://developers.google.com/identity/protocols/OAuth2ServiceAccount</a>');
		$this->attachView($field);

		$field = new TCv_FormItem_TextField('service_client_email', 'Client Email');
		$this->attachView($field);

		$field = new TCv_FormItem_TextField('service_client_id', 'Client ID');
		$this->attachView($field);

		$field = new TCv_FormItem_TextBox('service_private_key', 'Private Key');
		$this->attachView($field);

		$client = TC_initClass('TMm_GoogleAPI');
	}
	

}
?>