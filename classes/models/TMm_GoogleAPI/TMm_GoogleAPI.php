<?php

// include the autoloader
require_once($_SERVER['DOCUMENT_ROOT'].'/admin/GoogleAPI/classes/models/google-api-php-client-2.0.3/vendor/autoload.php');

// https://github.com/google/google-api-php-client
class TMm_GoogleAPI extends TCm_Model
{
	private $client_id;
	private $client_secret;
	private $google_client, $google_client_service, $google_client_apikey;


	/**
	 * TMm_GoogleAPI constructor.
	 * @param string $type (Optional) Default "oauth". Other options include "service" and "apikey"
	 */
	public function __construct($type = 'oauth')
	{
		parent::__construct(false);
		$this->client_id = TC_getModuleConfig('GoogleAPI', 'client_id');
		$this->client_secret = TC_getModuleConfig('GoogleAPI', 'client_secret');
		//$scopes = TC_getModuleConfig('google_api','scopes');

		if($this->client_id != '' && $this->client_secret != '')
		{
			$this->google_client = new Google_Client();
			$this->google_client->setClientId($this->client_id);
			$this->google_client->setClientSecret($this->client_secret);
			$this->google_client->setIncludeGrantedScopes(true);
			$this->google_client->setRedirectUri($this->fullDomainName() . '/admin/GoogleAPI/do/oauth2-callback');
			//$this->google_client->addScope(implode(',', $scopes));
		}


		// SERVICE SETTINGS CONFIGURED
		if(TC_getModuleConfig('GoogleAPI', 'service_private_key') != '')
		{
			$this->google_client_service = new Google_Client();

			// CONFIG is built from the module variables in the DB
			$config = array();
			$config['type'] = 'service_account';
			$config['client_id'] = TC_getModuleConfig('GoogleAPI', 'service_client_id');
			$config['client_email'] = TC_getModuleConfig('GoogleAPI', 'service_client_email');
			$config['private_key'] = str_replace('\n', "\n", TC_getModuleConfig('GoogleAPI', 'service_private_key'));
			$this->google_client_service->setAuthConfig($config);
		}


		if(TC_getModuleConfig('GoogleAPI', 'api_key') != '')
		{
			$this->google_client_apikey = new Google_Client();
			$this->google_client_apikey->setDeveloperKey(TC_getModuleConfig('GoogleAPI', 'api_key'));

		}
	}

	/**
	 * Returns the google client for direct access. If called and only one type is set, it will return that type
	 * cascading starting with the main oauth client
	 * @return Google_Client
	 */
	public function client()
	{
		if($this->google_client)
		{
			return $this->google_client;
		}
		elseif($this->google_client_service)
		{
			return $this->google_client_service;
		}
		elseif($this->google_client_apikey)
		{
			return $this->google_client_apikey;
		}

	}

	/**
	 * Returns the google client that is used for services
	 * @return Google_Client
	 */
	public function serviceClient()
	{
		return $this->google_client_service;
	}

	/**
	 * Redirects to an auth request with a provided scope
	 * @param int|array $scope
	 */
	public function sendAuthRequestWithScope($scope)
	{
		$this->google_client->addScope($scope);
		$auth_url = $this->google_client->createAuthUrl();
		header('Location: '.filter_var($auth_url, FILTER_SANITIZE_URL));
		exit();

	}

	/**
	 * Redirects to an auth request with a provided scope
	 * @param
	 */
	public function oauth2Callback()
	{
		//var_dump($_GET);
		if (! isset($_GET['code']))
		{
		//	print 'code not set';
			$auth_url = $this->google_client->createAuthUrl();
			header('Location: ' . filter_var($auth_url, FILTER_SANITIZE_URL));
		}
		else
		{
		//	print 'authenticating';
			$this->google_client->authenticate($_GET['code']);
			$access_token_array = $this->google_client->getAccessToken();
			$access_token = $access_token_array['access_token'];
//			foreach($access_token as $id => $value)
//			{
//				print '<br />'.$id . ' = ' . $value;
//			}
			//$this->addConsoleDebug($access_token);
			//print 'Token = ' . $access_token;
		//	exit();

			$user_id = 0;
			if(TC_currentUser())
			{
				$user_id = TC_currentUser()->id();
			}
			$query = "INSERT INTO google_oauth2 SET token = :token, user_id = :user_id, date_added = now()";
			$this->DB_Prep_Exec($query, array('token' => $access_token, 'user_id' => $user_id));
			//$redirect_uri = 'http://' . $_SERVER['HTTP_HOST'] . '/';
			//header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
		}
	}
}
?>