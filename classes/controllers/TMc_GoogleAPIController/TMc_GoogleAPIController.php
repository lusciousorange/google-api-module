<?php

/**
 * Class TMc_GoogleAPIController
 */
class TMc_GoogleAPIController extends TSc_ModuleController
{
	/**
	 * TMc_GoogleAPIController constructor.
	 * @param int|string|TSm_Module $module
	 */
	public function __construct($module)
	{
		parent::__construct($module);
		
	}

	/**
	 * Defines the URL targets
	 */
	public function defineURLTargets()
	{
		//parent::defineURLTargets();

		// Redirect to Settings
		/** @var TSm_ModuleURLTarget $target */
		$target = TC_initClass('TSm_ModuleURLTarget', '');
		$target->setNextURLTarget('settings');
		$this->addModuleURLTarget($target);

		// This is the URL that redirects should be sent to
		$target = TC_initClass('TSm_ModuleURLTarget', 'oauth2-callback');
		$target->setModelName('TMm_GoogleAPI');
		//$target->setModelInstanceRequired();
		$target->setModelActionMethod('oauth2Callback()');
		$target->setNextURLTarget(null);
		$target->setTitle('OAuth 2 Callback');
		$target->setAsSkipsAuthentication(); // can be accessed without Tungsten authentication
		$this->addModuleURLTarget($target);

	}
	


}
?>